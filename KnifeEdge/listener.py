"""listener.py - Listener classes for multiprocessing communication
"""


from PySide import QtCore
import time


class RunnerListener(QtCore.QThread):
    """RunnerListener observing a Runners communication pipe.

    RunnerListener observes the ``Runnner.con`` end of the duplex communication
    pipe and emits a notify signal as soon as a result from ``Runner.con`` is
    available. The the runner process ends, a terminated signal is emitted.
    """


    notify     = QtCore.Signal(object)  # emitted when result is available
    terminated = QtCore.Signal()        # emitted when runner has terminated


    def __init__(self, runner):
        """Constructor

        Args:
            runner (Runner): Instance of Runner to listen to
        """

        super(RunnerListener, self).__init__()

        self.runner = runner
        

    def run(self):
        """Listener loop observing ``Runner.con``

        As soon as an object is available on ``Runner.con`` recieve it and call
        the ``notify_res`` method with it.

        This ends when the ``Runner`` process terminates.
        """

        while self.runner.is_alive():
            # Need this small timeout here. Otherwise GUI is damn slow
            time.sleep(0.01)
            if (self.runner.con.poll()):
                self.notify.emit(self.runner.con.recv())

        self.terminated.emit()
        print('Listener ending')
