"""dataset.py - Bookkeeping class saving data of a measurement
"""

from collections import UserDict, UserList, namedtuple
import numpy as np
from scipy.optimize import curve_fit
from scipy.special import erf
from scipy.interpolate import UnivariateSpline
from scipy.stats import norm



def leastsq(x, y, func, pinit, yerr=None):
    """General purpose fitting routine for least-sqaures fitting

    Args:
        x (list): independent variable
        y (list): dependent variable
        func (callable): fit function
        pinit (list): initial parameters values
        yerr (list): uncertainties in dependent variable

    Returns:
        list, list: final fit parameters and standard errors
    """

    try:
        out, cov = curve_fit(func, x, y,
                             p0     = pinit,
                             sigma  = yerr,
                             maxfev = 50000,
                            )
    except ValueError:
        print("Input data for fit not valid")
        return None, None

    return out, np.sqrt(np.diag(cov))





class Value(namedtuple('Value', ['value', 'uncert'])):
    """Namedtuple representing a value with uncertainties"""

    def __str__(self):
        """Overwrite: string representation"""

        return "%.4f +/- %.4f" % self


    def __float__(self):
        """Make Value castable to float"""
        return float(self.value)





class RunData(UserDict):
    """Bookkeeping class saving data of a measurement run
    """

    @classmethod
    def empty(cls):
        """Create and empty RunData instance with default fields"""

        _default_fields = {'position'    : []
                          ,'intensity'   : []
                          ,'orientation' : None
                          ,'zpos'        : None
                          ,'width'       : None
                          ,'width9010'   : None
                          }

        return RunData(_default_fields)


    @classmethod
    def fake(cls, p=[0,1000,12.5,4], noise=None, num=150, range_=(0,25),
             orientation='V'):
        """Create a RunData instance with fake data

        Args:
            p (list): list of parameters to pass to the intensity function
            noise (float): optional noise contribution
            num (int): length of dataset
            range_ (tuple): start and end point of dataset
            orientation (str): must be either 'V' or 'H'
        """

        d = RunData.empty()

        x = np.linspace(range_[0], range_[1], num)
        if noise:
            _noise = np.random.normal(scale=noise,size=num)
        else:
            _noise = np.zeros(num)

        d['position']     = list(x)
        d['intensity']    = list( cls.func_width(x, *p) + _noise )
        d['destinations'] = list(x)
        d['orientation']  = orientation
        d['zpos']         = 3.14
        d['truth_offset'] = p[0]
        d['truth_power']  = p[1]
        d['truth_width']  = p[3]
        d['truth_mean']   = p[2]

        return d


    def __init__(self, dict_=None):
        """Constructor

        Args:
            dict_ (dict): Create RunData from existing dictionary
        """

        super(RunData, self).__init__(dict_)


    def __str__(self):
        """String representation"""

        ret = "<RunData @ z=%.4f in %s-direction with %d datapoints>" \
                % (self.data['zpos'], self.data['orientation'], len(self))
        return ret


    def __len__(self):
        """Return length of the dataset"""
        return len(self.data['position'])


    def saveToFile(self, filepath, additional=None):
        """Save this dataset to a csv file

        Calling this method assignes `filepath` to a new field in self.data

        Args:
            filepath (str): file path for writing
            additional (list): keys additional to defaults fields to write
        """

        # Create header of file
        header  = " " + filepath + "\n\n"

        for key in ['orientation', 'zpos', 'width', 'width9010']:
            header += " " + "%s: %s\n" % (key, str(self.data[key]))
        header += " " + "\n"

        if additional:
            for key in additional:
                header += " " + "%s: %s\n" % (key, str(self.data[key]))
            header += " " + "\n"

        header += " " + "position,intensity\n"
        header += "-"*78

        np.savetxt(filepath, np.transpose((self.data['position'],
                                           self.data['intensity'])),
            fmt      = '%.4f,%.4f',
            header   = header,
            comments = '#'
            )


    def addWidthFit(self):
        """Perform a fit on this dataset and store the parameters and function
        """

        if len(self.data['position']) < 5:
            print('Too view datapoints to fit width')
            return

        pinit = [ np.min(self.data['intensity'])
                , np.max(self.data['intensity'])
                , np.mean(self.data['position'])
                , (self.data['position'][-1] - self.data['position'][0])
                ]

        pout, perr = leastsq(self.data['position'], self.data['intensity'],
                               func  = self.func_width,
                               pinit = pinit,)

        if pout is not None and perr is not None:
            self.data['width'] = Value(pout[3], perr[3])
            self.data['func']  = self.func_width
            self.data['pout']  = pout
            self.data['perr']  = perr


    def addWidth9010(self):
        """Determine the width by the 90%-10% clipping method"""

        if len(self.data['position']) < 10:
            print('Too view datapoints to estimate width9010')
            return

        # Estimate max/min/noise using the first/last 10 data points
        noise = np.std (self.data['intensity'][-10:])
        I_min = np.mean(self.data['intensity'][:10] )
        I_max = np.mean(self.data['intensity'][-10:])

        if noise < 0.1:
            noise = 1.0

        # set weights to determine number of knots correctely
        #   see: http://docs.scipy.org/doc/scipy/reference/generated/\
        #        scipy.interpolate.UnivariateSpline.html
        w = np.ones_like(self.data['intensity']) / noise

        spl = UnivariateSpline(self.data['position'],
                               self.data['intensity'],
                               k=3,
                               w=w,
                               ext=3)

        x = np.linspace(self.data['position'][0],
                        self.data['position'][-1],
                        100000)
        y = spl(x)
        y_10 = I_min + (I_max-I_min) * 0.1
        y_90 = I_min + (I_max-I_min) * 0.9
        x_10 = x[ np.argwhere(y > y_10)[ 0] ]
        x_90 = x[ np.argwhere(y < y_90)[-1] ]
        width9010  = float((x_90 - x_10) / norm.ppf(0.9))

        dpos = self.data['position'][1] - self.data['position'][0]
        dwidth9010 = ( float((x_90 - x_10 + dpos)) / norm.ppf(0.9) - width9010 ) / 2.0

        self.data['width9010'] = Value(width9010, dwidth9010)


    @staticmethod
    def func_width(x, c, P, mu, w):
        """The fit function for width estimation

        Args:
            x (float): independent variable
            c (float): constant offset
            P (float): scaling parameter
            mu (float): location parameter
            w (float): width parameter

        Returns:
            float: intensity at given x
        """
        return c + P/2. * (1+ erf( np.sqrt(2) * (x - mu) / w ))





class RunDataList(UserList):
    """Bookkeeping class for RunData instances"""

    def __init__(self, datasets=[]):
        """Constructor

        Args:
            datasets (list): list of RunData instances
        """

        super(RunDataList, self).__init__(datasets)

        self.results = {}


    @classmethod
    def fake(cls, p=[1.0,2.0,2.5], num=15, range_=(0,5)):
        """Create a RunDataList instance with fake data

        Args:
            p (list): list of parameters to pass to the waist function
            num (int): number of datasets
            range_ (tuple): start and end point along z

        Returns:
            RunDataList: instance of RunDataList with fake data
        """

        datasets = []

        z = np.linspace(range_[0], range_[1], num)
        for (zpos,orientation) in [(zpos_,orientation_)
                                  for zpos_ in z
                                  for orientation_ in ['V','H']]:
            w = cls.func_waist(zpos, *p)
            dataset = RunData.fake(p           = [0,1000,12.5,w],
                                   noise       = 1.0,
                                   range_      = (12.5 - 2*w, 12.5 + 2*w),
                                   orientation = orientation)
            dataset['zpos'] = float(zpos)
            dataset['filepath'] = '/dev/null'
            dataset.addWidthFit()
            dataset.addWidth9010()
            datasets.append(dataset)

        return RunDataList(datasets)


    def saveToFile(self, filepath):
        """Save list of datasets in a csv file

        Args:
            filepath (str): path to save the file to
        """

        if len(self.data) == 0:
            return

        header  = filepath + "\n\n"
        header += 'orientation,zpos,width,dwidth,width9010\n'
        header += "-"*78
        header += "\n"

        orientation = [dset['orientation']  for dset in self.data]
        zpos        = [dset['zpos']         for dset in self.data]
        width       = [dset['width'].value  for dset in self.data]
        dwidth      = [dset['width'].uncert for dset in self.data]
        width9010   = [dset['width9010']    for dset in self.data]

        with open(filepath, 'w') as out:
            out.write(header)

            for i,_ in enumerate(orientation):
                out.write("%s,%.4f,%.4f,%.4f,%.4f\n" % (orientation[i],
                                                        zpos[i],
                                                        width[i],
                                                        dwidth[i],
                                                        width9010[i]))


    def addWaistFit(self, key='width'):
        """Fit a waist function through the widths in datasets

        Args:
            key (str): key in RunData pointing to the width to use
        """

        for VorH in ['V','H']:

            zpos  = [dset['zpos'] for dset in self.data \
                    if dset['orientation'] == VorH]
            width = [dset[key]    for dset in self.data \
                    if dset['orientation'] == VorH]

            if len(zpos) < 3:
                print('Not enough data to fit waist in %s direction. %d < 3' \
                        % (VorH, len(zpos)))
                continue

            if type(width[0]) == Value:
                dwidth = [w.uncert for w in width]
                width  = [w.value  for w in width]
            else:
                dwidth = None

            pinit = [np.min(width), 0.1, 2.5] # maybe make this dynamic

            pout, perr = leastsq(zpos, width,
                                   func  = self.func_waist,
                                   pinit = pinit,
                                   yerr  = dwidth,)

            if pout is not None and perr is not None:
                self.results['pout' +VorH] = pout
                self.results['perr' +VorH] = perr
                self.results['func' +VorH] = self.func_waist
                self.results['waist'+VorH] = Value(pout[0],perr[0])


    @staticmethod
    def func_waist(z, w0, zR, z0):
        """Waist function to fit beam waists

        Args:
            z (float): independent variable
            w0 (float): waist radius
            zR (float): rayleigh-length
            z0 (float): longitudinal offset

        Returns:
            float: width at given z

        """
        return w0 * np.sqrt( 1 + ((z-z0)/zR)**2 )





class NoiseData(UserDict):
    """Bookkeeping class for saving data of noise measurements
    """

    @classmethod
    def empty(cls):
        """Create and empty NoiseData instance with default fields"""

        _default_fields = {'intensity'   : [],
                          }

        return NoiseData(_default_fields)


    @classmethod
    def fake(cls, num=1000, noise=5.0):
        """Create a fake NoiseData set

        Args:
            num (TODO): TODO
            noise (TODO): TODO

        Returns: TODO

        """

        d = NoiseData()

        intensity = list(np.random.normal(loc   = 950,
                                          scale = noise,
                                          size  = num))
        d.data['intensity'] = intensity

        return d


    def __init__(self, dict_=None):
        """Constructor

        Args:
            dict_ (dict): Create NoiseData from existing dictionary
        """

        super(NoiseData, self).__init__(dict_)


    def __str__(self):
        """String representation"""

        return "<NoiseData w/ %d datapoints>" % len(self)


    def __len__(self):
        """Return length of the dataset"""
        return len(self.data['intensity'])


    def saveToFile(self, filepath):
        """Save this dataset to a csv file

        Calling this method assignes `filepath` to a new field in self.data

        Args:
            filepath (str): file path for writing
        """

        self.data['filepath'] = filepath

        # Create header of file
        header  = " " + filepath + "\n\n"

        if 'sigma' in self.data:
            header += " " + "sigma = %s" % str(self.data['sigma'])

        header += " " + "intensity\n"
        header += "-"*78

        np.savetxt(filepath, np.transpose((self.data['intensity'],)),
            fmt = '%.4f',
            header = header,
            comments = '#'
            )


    def addGaussianFit(self):
        """Perform a gaussian fit on the noise data"""

        I = self.data['intensity']

        binning = np.arange(np.min(I)-1, np.max(I)+3, step=1)
        bin_cen = binning[:-1] + 0.5

        hist,_ = np.histogram(I, bins=binning)
        pois   = np.sqrt(hist)
        pois[ pois == 0 ] = 1

        pinit = [np.max(hist), np.mean(I), np.std(I)]

        pout, perr = leastsq(bin_cen, hist,
                               func  = self.func_gauss,
                               pinit = pinit,
                               yerr  = pois,)

        if pout is not None and perr is not None:
            self.data['sigma'] = Value(pout[2],perr[2])
            self.data['pout'] = pout
            self.data['perr'] = perr
            self.data['func'] = self.func_gauss


    @staticmethod
    def func_gauss(x, norm, mean, std):
        """Gaussian fit function

        Args:
            x (float): independent variable
            norm (float): normalization parameter
            mean (float): location parameter
            std (float): scale parameter

        Returns:
            float: gaussian pdf at requesets position
        """

        return norm * np.exp( -0.5 * ((mean-x)/std)**2  )
