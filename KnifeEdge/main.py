"""main.py - QtWidget for the main user interface
"""

import os
from glob import glob
from PySide import QtGui, QtCore

from .ui.ui_main import UI_Main
from .measurement import Measurement
from .noise import Noise
from .dataset import RunDataList



class Main(QtGui.QWidget):
    """QtWidget for the main user interface"""

    def __init__(self):
        """Constructor"""

        super(Main, self).__init__()

        self.config = QtCore.QCoreApplication.instance().config
        self.fileIO = QtCore.QCoreApplication.instance().fileIO

        self.ui = UI_Main()
        self.ui.initUI(self)

        self.runDataList = RunDataList()

        if self.config.getboolean('general','demo'):
            self.runDataList = RunDataList.fake()
            self.runDataListChanged()


    def newRun(self):
        """Start a new measurment run"""

        m = Measurement()
        m.saved.connect(self.addMeasurement)

    
    def noiseRun(self):
        """Start a new noise run"""

        noise = Noise()


    def addMeasurement(self, d):
        """Add a RunData instance to the list

        Args:
            d (RunData): instance of RunData to add
        """
        self.runDataList.append(d)
        self.runDataListChanged()


    def delMeasurement(self, index):
        """Delete a RunData instance from the list

        Args:
            index (int): table row index of the dataset
        """
        print('Deleting %d' % index)
        
        basename = self.runDataList[index]['filepath']
        for f in glob(basename+'.*'):
            os.remove(f)
            
        del self.runDataList[index]
        
        self.runDataListChanged()


    def showRunData(self, dataset):
        """Show a previous measurements dataset.
        
        Args:
            dataset (RunData): Instance of RunData to show
        """
        m = Measurement(dataset)


    def saveWidths(self):
        """Write a 'widths file' containing all fitted widths
        """
        
        filename = self.fileIO.filepath() + '_widths.csv'
        self.runDataList.saveToFile(filename)
        print('Saved widthFile: %s' % filename)
    
        if self.config.getboolean('general','demo'):
            for dataset in self.runDataList:
                filename = self.fileIO.filepath() + "_%s%.4f.csv" \
                    % (dataset['orientation'], dataset['zpos'])
                dataset.saveToFile(filename,
                                   additional=['truth_width',
                                               'truth_mean',
                                               'truth_offset',
                                               'truth_power'])
                print('Saved file: %s' % filename)


    def saveFig(self):
        """Save the current WaistPlot figure"""

        filepath = self.fileIO.filepath() + '_waist.pdf'
        self.ui.plot.saveToFile( filepath )


    def runDataListChanged(self):
        """Update WaistPlot and availability of buttons when the list of
        datasets has changed.
        """

        if len(self.runDataList) == 0:
            self.ui.button_SaveWidths.setEnabled(False)
            self.ui.button_FitWaist.setEnabled(False)
            self.ui.button_SaveFig.setEnabled(False)
        else:
            self.ui.button_SaveWidths.setEnabled(True)
            self.ui.button_FitWaist.setEnabled(True)
            self.ui.button_SaveFig.setEnabled(True)

        self.ui.plot.plot(self.runDataList,
                          key=self.config.get('general','waistBasedOn'))

        self.ui.table.model().datasets = self.runDataList


    def fitWaist(self):
        """Fit the waist based on the data"""

        self.runDataList.addWaistFit(key=self.config.get('general','waistBasedOn'))

        for VorH in ['V','H']:
            if 'waist'+VorH in self.runDataList.results:
                print(VorH, self.runDataList.results['waist'+VorH])
        
        self.ui.plot.plot(self.runDataList,
                          key=self.config.get('general','waistBasedOn'))


    def closeEvent(self, event):
        """Overwrite behaviour on closing the window"""

        reply = QtGui.QMessageBox.question(self, 'Message',
                    "Are you sure you want to quit?",
                    QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                    QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()
