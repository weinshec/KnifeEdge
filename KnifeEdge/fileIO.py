"""fileIO.py - Manage file IO operations on os level
"""

import os
import datetime
from PySide import QtCore


class FileManager(object):
    """Manager class for file IO operations"""

    def __init__(self):
        """Constructor"""

        self.config   = QtCore.QCoreApplication.instance().config
        self._ts_init = datetime.datetime.now()

        if not os.path.exists( self.basepath ):
            os.mkdir( self.basepath )


    @property
    def basepath(self):
        """Return the basepath for file saving"""
        
        dir_ = self.config.get('general', 'path')
        dir_ = os.path.expandvars(dir_)
        dir_ = os.path.expanduser(dir_)

        base_dir = "%04d_%02d_%02d_%02d%02d" % (int(self._ts_init.year)
                                               ,int(self._ts_init.month)
                                               ,int(self._ts_init.day)
                                               ,int(self._ts_init.hour)
                                               ,int(self._ts_init.minute)
                                               )

        return os.path.join(dir_,base_dir)


    def filepath(self):
        """Create a generic filepath without extension

        Returns:
            str: filepat (absolute) coded with timestemp w/ extension
        """

        t = datetime.datetime.now()

        name  = ""
        name += "%04d%02d%02d_%02d%02d%02d" \
            % (t.year, t.month, t.day, t.hour, t.minute, t.second)

        return os.path.join(self.basepath, name)


    def cleanUp(self):
        """Remove empty directories"""

        if not os.listdir( self.basepath ):
            os.rmdir( self.basepath )


    def delete(self, filepath):
        """Delete a file under given filepath

        Args:
            filepath (str): file to delete
        """

        try:
            os.remove( filepath )
        except:
            print('fileIO.delete: File not found %s' % filepath )
        
