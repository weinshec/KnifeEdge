"""
KnifeEdge - control application for the Knife-Edge-Unit

This is a more sophisticated version of the Knife-Edge-Unit control application
"""

__author__  = "Christoph Weinsheimer"
__email__   = "christoph.weinsheimer@desy.de"
__version__ = "2.0.0"

