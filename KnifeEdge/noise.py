"""noise.py - QtWidget performing a noise run
"""

from PySide import QtGui, QtCore
import numpy as np
import os

from pyAPT import ControllerStatus

from .ui.ui_noise import UI_Noise
from .runner import Runner
from .listener import RunnerListener
from .dataset import NoiseData



class Noise(QtGui.QWidget):
    """QtWidget performing a noise run"""


    saved = QtCore.Signal(NoiseData)  # emitted when the dataset has been saved


    def __init__(self):
        """Constructor"""

        super(Noise, self).__init__()

        self.config   = QtCore.QCoreApplication.instance().config
        self.fileIO   = QtCore.QCoreApplication.instance().fileIO
        self.runner   = None
        self.listener = None
        self.data     = None

        self.ui = UI_Noise()
        self.ui.initUI(self)

        self.maxNum     = None
        self.saturation = None


    def run(self):
        """Start a noise measurement"""
        
        if self.config.getboolean('general','demo'):
            self.data = NoiseData.fake()
            self.notify_runnerEnd()
            return
        else:
            self.data = NoiseData().empty()

        if self.ui.options.check_NumPoints.isChecked():
            self.maxNum = self.ui.options.numPoints
        else:
            self.maxNum = None
        
        self.ui.enableControls(False)
        self.runner   = Runner(self.config, stage=None)
        self.listener = RunnerListener(self.runner)
        self.listener.notify.connect(self.notify)
        self.listener.terminated.connect(self.notify_runnerEnd)

        self.runner.start()
        self.listener.start()
        self.runner.con.send('cr')
        


    def stop(self):
        """Stop the Runner process"""
        
        if self.runner:
            self.runner.term()


    def save(self):
        """Save the dataset and dismiss the Measurement widget"""
            
        if self.data is None:
            return
        else:
            filebasename = self.fileIO.filepath() + "_noise"

            self.data        .saveToFile( filebasename + '.csv'      )
            self.ui.plot     .saveToFile( filebasename + '_line.pdf' )
            self.ui.histogram.saveToFile( filebasename + '_hist.pdf' )

            self.saved.emit(self.data)
            self.close()


    def closeEvent(self, event):
        """Overwrite behavior on closing the window"""
        
        print('Noise close event')
        
        if self.data is None:
            event.accept()
        
        elif 'filepath' not in self.data:     # Means data is not yet saved
            reply = QtGui.QMessageBox.question(self, 'Message',
                        "Really discard this measurment",
                        QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                        QtGui.QMessageBox.No)
            if reply == QtGui.QMessageBox.Yes:
                event.accept()
            else:
                event.ignore()
        else:
            event.accept()


    def notify(self, res):
        """Notify method to call when a new result is ready
        
        Args:
            res (object): the result from the device command executed by runner
        """

        self.data['intensity'].append(res)

        length = len(self.data['intensity'])

        if self.maxNum == length:
            self.stop()

        if length % 10 == 0: # Update plots every 10 calls
            self.ui.plot.plot(self.data)
            self.ui.histogram.histogram(self.data)


    def notify_runnerEnd(self):
        """Notify method to call when the runner ended."""

        self.data.addGaussianFit()
        
        self.ui.enableControls(True)
        self.ui.plot.plot(self.data)
        self.ui.histogram.histogram(self.data)


    @property
    def filepath(self):
        """Filepath of the this runs dataset"""
    
        if self.data is None:
            return "-"
        elif 'filepath' in self.data:
            return os.path.basename(self.data['filepath'])


    def setSaturation(self, state):
        """Set showing of the saturation level
        
        Args:
            state (bool): show saturation level or not
        """

        if state:
            self.ui.plot.setSaturation(
                    self.config.getfloat('arduino','saturation'))
        else:
            self.ui.plot.setSaturation( None )
