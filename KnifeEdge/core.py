"""core.py - Core class of the KnifeEdge unit package
"""

import sys
import os
import time
from PySide import QtGui
from pkg_resources import Requirement, resource_filename
import warnings

from .main import Main
from .config import Config
from .fileIO import FileManager
from .runner import Runner


class Core(QtGui.QApplication):
    """Core class extending QApplication"""


    def __init__(self, argv):
        """Constructor

        Parameters:
        argv : sys.argv
            commandline parameters
        """

        # Call super constructor
        super(Core, self).__init__(argv)
        self.lastWindowClosed.connect(self.shutdown)

        configFile = resource_filename(
                Requirement.parse("KnifeEdge"),"KnifeEdge.conf")
        self.config = Config(configFile)
        self.fileIO = FileManager()

        self.init_devices()

        warnings.simplefilter("ignore")

        self.mainWindow = Main()


    def init_devices(self):
        """Initialize all devices"""

        if self.config.getboolean('general','demo'):
            return

        print('Initializing devices...')

        runners    = []
        _not_found = []

        if not os.path.exists(self.config.get('arduino','port')):
            _not_found.append('arduino (%s)' % self.config.get('arduino','port'))

        for stage in ['stageH','stageV','stageZ']:
            if self.config.getboolean(stage, 'active'):
                try:
                    runner = Runner(self.config, stage, noread=True)
                    runners.append(runner)
                except Exception as e:
                    print(e)
                    _not_found.append('%s (SN: %s)'
                                      % (stage, self.config.get(stage,'serial')))
                    continue


        if _not_found:
            errMsg = "The following devices could not be initialized!\n\n"
            for device in _not_found:
                errMsg += "   - %s\n" % device
            QtGui.QMessageBox.critical(None,
                                       'Initialization Error',
                                       errMsg,
                                       QtGui.QMessageBox.Close,
                                       QtGui.QMessageBox.NoButton)
            sys.exit(1)

        for runner in runners:
            runner.start()
            runner.con.send('s')
            status = runner.con.recv()
            if not status.homed:
                print('homing stage')
                runner.con.send('h')
            runner.con.send('q')

        while any( [runner.is_alive() for runner in runners] ):
            time.sleep(0.01)

        print('...done')


    def shutdown(self):
        """Shutdown routine

        Close all device connections
        """

        self.fileIO.cleanUp()



def start_gui():
    """Create an instance of Core"""
    core = Core(sys.argv)
    sys.exit(core.exec_())



if __name__ == '__main__':
    start_gui()
