"""config.py - Config file parser class
"""


from configparser import ConfigParser


_mandatory = {'general' : ['path','demo','waistBasedOn']
             ,'stageV'  : ['active','serial','model','scale']
             ,'stageH'  : ['active','serial','model','scale']
             ,'stageZ'  : ['active','serial','model','scale']
             ,'arduino' : ['port']
             }


class Config(ConfigParser):
    """Config file parser for the KnifeEdge"""

    def __init__(self, filename):
        """Constructor
        
        Args:
            filename (str): filename of config file
        """

        super(Config, self).__init__()

        self.filename = filename
        self.read(filename)

        self.checkConsistency()


    def checkConsistency(self):
        """Check if all mandatory sections and options are given

        Raises:
            RuntimeError: in case any section or option is missing
        """

        for section in _mandatory.keys():
            if not self.has_section(section):
                raise RuntimeError('Section [%s] missing in config file %s'\
                        % (section, self.filename))

        for section, options in _mandatory.items():
            for option in options:
                if not self.has_option(section,option):
                    raise RuntimeError(
                        'Option "%s" missing in section [%s] of configfile %s'\
                        % (option, section, self.filename)
                        )
