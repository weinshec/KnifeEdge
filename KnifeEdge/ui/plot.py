"""plot.py - Implement dynamic plot widget using matplotlib
"""

from PySide import QtGui, QtCore

import numpy as np
import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4']='PySide'
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

from ..dataset import Value



class PlotWidget(FigureCanvas):
    """QtWidget for dynamic plotting using matplotlib"""

    def __init__(self, width=5.5, height=4, dpi=90):
        """Constructor"""

        # Create figure and axes instance
        self.fig = Figure(figsize      = (width, height),
                          dpi          = dpi,
                          facecolor    = 'white',
                          tight_layout = True,
                          )
        self.axes = self.fig.add_subplot(111)

        # We want the axes cleared every time plot() is called
        self.axes.hold(False)

        self.compute_initial_figure()

        # Create the QWidget inheritent
        FigureCanvas.__init__(self, self.fig)
        FigureCanvas.setSizePolicy(self,
                                   QtGui.QSizePolicy.Expanding,
                                   QtGui.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)


    def compute_initial_figure(self):
        """Needs to be called to show plot correctly"""
        pass


    def plot(self, x, y):
        """Basic plot method, plotting y vs. x

        Args:
            x (list): independent variable
            y (list): dependent variable
        """

        self.axes.plot(x,y)
        self.draw()


    def saveToFile(self, filepath):
        """Save this plot to a file

        The filepath extension defines the output format (e.g. .pdf)

        Args:
            filepath (str): filepath to save the figure to
        """

        self.fig.savefig(filepath)





class RunDataPlot(PlotWidget):
    """QtWidget extending PlotWidget specialized on visualizing RunData"""


    def __init__(self, parent, *args):
        """Constructor

        Args:
            parent (QWidget): the parent widget (mandatory due to prevent this
                instance from being garbage collected)
        """

        super(RunDataPlot, self).__init__(*args)

        self.parent = parent
        self.config = QtCore.QCoreApplication.instance().config

        self.__applyStyle()
        self.axes.set_xlim(0,25)

        self.dataset = None
        self.lower   = 0
        self.upper   = 25


    def __applyStyle(self):
        """Set the plot style"""

        self.axes.set_xlabel('position / mm')
        self.axes.set_ylabel('intensity / a.u.')
        self.axes.grid()


    def __repaint(self):
        """Draw the whole figure
        """

        if not self.dataset:
            return

        common_length = min(len(self.dataset['position']),
                            len(self.dataset['intensity']))

        self.axes.plot(self.dataset['position'][:common_length],
                       self.dataset['intensity'][:common_length],
                       linestyle = '',
                       marker    = '+',
                       zorder    = 2,
                       )

        self.axes.hold(True)
        self.axes.axvline(self.lower, linestyle='--')
        self.axes.axvline(self.upper, linestyle='--')

        if 'func' in self.dataset and 'pout' in self.dataset:
            x = np.linspace(self.dataset['position'][0],
                            self.dataset['position'][-1],
                            100)
            self.axes.plot(x, self.dataset['func'](x, *self.dataset['pout']),
                           zorder=1)
            waistBasedOn = self.config.get('general', 'waistBasedOn')
            self.axes.text(0.05, 0.9, "w = %.4f +/- %.4f" \
                               % self.dataset[waistBasedOn],
                           transform=self.axes.transAxes,
                           bbox={'facecolor':'white',
                                 'alpha':0.8,
                                 'linewidth':0.0}
                           )

        self.__applyStyle()
        self.axes.set_xlim(self.dataset['destinations'][0],
                           self.dataset['destinations'][-1])
        yspan = (np.min(self.dataset['intensity']),
                 np.max(self.dataset['intensity']))
        dy    = yspan[1] - yspan[0]
        self.axes.set_ylim(yspan[0]-0.10*dy, yspan[1]+0.1*dy)

        self.axes.hold(False)
        self.draw()


    def plot(self, dataset):
        """Set the RunData dataset and call repaint

        Args:
            dataset (RunData): instance of RunData to plot
        """
        self.dataset = dataset
        self.__repaint()


    def setLowerMarker(self, xpos):
        """Set the lower bound marker and call repaint

        Args:
            xpos (float): position of lower bound marker
        """
        self.lower = xpos
        self.__repaint()


    def setUpperMarker(self, xpos):
        """Set the upper bound marker and call repaint

        Args:
            xpos (float): position of upper bound marker
        """
        self.upper = xpos
        self.__repaint()





class WaistPlot(PlotWidget):
    """QtWidget extending PlotWidget specialized on visualizing Waists"""


    def __init__(self, *args):
        """Constructor"""

        super(WaistPlot, self).__init__(*args)

        config   = QtCore.QCoreApplication.instance().config
        self._ylabel = config.get('general', 'waistBasedOn')

        self.__applyStyle()
        self.axes.set_xlim(0,50)


    def __applyStyle(self):
        """Set the plot style"""

        self.axes.set_xlabel('z-position / mm')
        self.axes.set_ylabel(self._ylabel + ' / mm')
        self.axes.grid()


    def plot(self, datasets, key='width'):
        """Draw the widths from a list of datasets

        Args:
            datasets (list): list of RunData containing widths
            key (str): key in RunData pointing to width
        """

        self.axes.clear()
        self.axes.hold(True)

        waistTxtOffset = 0.0

        for VorH in ['V', 'H']:
            x = [dataset['zpos'] for dataset in datasets
                    if dataset['orientation'] == VorH]
            y = [dataset[key] for dataset in datasets
                    if dataset['orientation'] == VorH]

            if len(x) == 0:
                continue

            if type(y[0]) == Value:
                dy = [v.uncert for v in y]
                y  = [v.value  for v in y]
            else:
                dy = None

            color = 'blue' if VorH == 'V' else 'red'

            self.axes.errorbar(x, y,
                           yerr      = dy,
                           linestyle = '',
                           marker    = 'o',
                           color     = color,
                           zorder    = 2,
                           )

            if 'waist'+VorH in datasets.results:
                low, hi = self.axes.get_xlim()
                x = np.linspace(low,hi,200)
                y = datasets.func_waist(x,*datasets.results['pout'+VorH])
                self.axes.plot(x, y,
                               linestyle = '--',
                               color     = color)

                waistTxt = r'$w_0^%s = %.4f \pm %.4f$' \
                        % (VorH,
                           datasets.results['waist'+VorH].value,
                           datasets.results['waist'+VorH].uncert)
                self.axes.text(0.5, 0.94-waistTxtOffset, waistTxt,
                               horizontalalignment = 'center',
                               verticalalignment   = 'center',
                               transform           = self.axes.transAxes)
                waistTxtOffset = 0.1


        self.__applyStyle()

        self.axes.hold(False)
        self.draw()





class NoiseDataPlot(PlotWidget):
    """QtWidget extending PlotWidget specialized on visualizing Noise Data"""

    def __init__(self, *args):
        """Constructor"""

        super(NoiseDataPlot, self).__init__(*args)

        self.saturation = None

        self.axes.grid()


    def plot(self, dataset):
        """Draw the noise dataset

        Args:
            dataset (NoiseData): instance of NoiseData to plot
        """

        x = np.arange(len(dataset['intensity']))

        self.axes.plot(x , dataset['intensity'],
                       linestyle = '',
                       marker    = 'o',
                       )
        if self.saturation:
            self.axes.hold(True)
            self.axes.axhline(self.saturation, linestyle='--')
            self.axes.hold(False)

        self.axes.grid()
        self.axes.set_xlabel('index')
        self.axes.set_ylabel('intensity / a.u.')
        self.axes.set_ylim(np.min(dataset['intensity']) - 3,
                           np.max(dataset['intensity']) + 3)

        self.draw()


    def histogram(self, dataset):
        """Draw the noise dataset as histogram

        Args:
            dataset (NoiseData): instance of NoiseData to plot
        """

        I = dataset['intensity']

        binning = np.arange(np.min(I)-1, np.max(I)+3, step=1)

        self.axes.hist(dataset['intensity'],
                       histtype = 'stepfilled',
                       bins     = binning,)

        if 'sigma' in dataset:
            self.axes.hold(True)

            low, hi = self.axes.get_xlim()
            x = np.linspace(low,hi,200)

            self.axes.plot(x, dataset.func_gauss(x,*dataset['pout']),
                           linestyle = '--',
                           color     = 'red')

            self.axes.hold(False)

        self.axes.grid()
        self.axes.set_xlabel('intensity / a.u.')
        self.axes.set_ylabel('counts')

        self.draw()


    def setSaturation(self, value):
        """Set saturation line plotting"""

        self.saturation = value
