"""ui_measurement.py - GUI definitions for the Measurement class
"""

import os
import sys
from PySide import QtGui, QtCore
from pkg_resources import Requirement, resource_filename

from .plot import RunDataPlot
from .table import RunDataTableModel, RunDataTableView



class UI_Measurement(object):
    """GUI definitions for the Measurement class"""

    def initUI(self, Measurement):
        """Initialize the UI"""

        self.parent = Measurement

        #
        # Button Box
        #
        self.button_Run     = QtGui.QPushButton('Run'    , Measurement)
        self.button_Preview = QtGui.QPushButton('Preview', Measurement)
        self.button_Stop    = QtGui.QPushButton('Stop'   , Measurement)

        vBox_buttons = QtGui.QVBoxLayout()
        vBox_buttons.setContentsMargins(10,10,10,10)
        vBox_buttons.setSpacing(10)
        vBox_buttons.addWidget(self.button_Run)
        vBox_buttons.addWidget(self.button_Preview)
        vBox_buttons.addWidget(self.button_Stop)
        vBox_buttons.addStretch(1)
        self.button_Run.setFixedSize(100,100)
        self.button_Preview.setFixedSize(100,40)
        self.button_Stop.setFixedSize(100,40)

        self.button_Run.clicked.connect(Measurement.run)
        self.button_Preview.clicked.connect(Measurement.preview)
        self.button_Stop.clicked.connect(Measurement.stop)
        

        #
        # Limit Box
        #
        self.slider_Start = Measurement_Slider('Start',  0.0, (0.0,25.0))
        self.slider_Stop  = Measurement_Slider('Stop' , 25.0, (0.0,25.0))

        vBox_Limits = QtGui.QVBoxLayout()
        vBox_Limits.addWidget(self.slider_Start)
        vBox_Limits.addWidget(self.slider_Stop)
        groupBox_limits = QtGui.QGroupBox('Limits')
        groupBox_limits.setLayout(vBox_Limits)


        #
        # Position Box
        #
        self.positionBox = Measurement_PositionBox(default=0.0)


        #
        # Step Box
        #
        self.stepBox = Measurement_StepBox(self.slider_Start.value,
                                           self.slider_Stop.value,
                                           default = 1.0)
        self.slider_Start.valueChanged.connect(self.stepBox.startValueChanged)
        self.slider_Stop.valueChanged.connect(self.stepBox.stopValueChanged)


        #
        # Orientation Box
        #
        self.orientationBox = Measurement_OrientationBox()


        #
        # Save File Box
        #
        self.button_Save    = QtGui.QPushButton('', Measurement)
        self.button_Cancel  = QtGui.QPushButton('', Measurement)
        self.label_filepath = QtGui.QLabel(Measurement.filepath)

        save_icon = resource_filename(
            Requirement.parse("KnifeEdge"),
            "KnifeEdge/ui/icons/Button-Ok-icon.png"
        )
        cancel_icon = resource_filename(
            Requirement.parse("KnifeEdge"),
            "KnifeEdge/ui/icons/Button-Delete-icon.png"
        )
        self.button_Save.setIcon(QtGui.QIcon(save_icon))
        self.button_Cancel.setIcon(QtGui.QIcon(cancel_icon))

        hBox_save = QtGui.QHBoxLayout()
        hBox_save.setContentsMargins(10,10,10,10)
        hBox_save.addSpacing(15)
        hBox_save.addWidget(self.button_Cancel)
        hBox_save.addSpacing(10)
        hBox_save.addWidget(self.button_Save)
        hBox_save.addSpacing(30)
        hBox_save.addWidget(self.label_filepath)
        self.button_Save.setFixedSize(25,25)
        self.button_Cancel.setFixedSize(25,25)

        self.button_Cancel.clicked.connect(Measurement.close)
        self.button_Save.clicked.connect(Measurement.save)


        #
        # Table Box
        #
        model = RunDataTableModel(keys=['orientation'
                                       ,'zpos'
                                       ,'width'
                                       ,'width9010'
                                       ])
        self.table = RunDataTableView(model)

        self.table.setFixedHeight(65)
        self.table.verticalHeader().hide()


        #
        # Plot Widget
        #
        self.plot = RunDataPlot(parent=self)
        self.slider_Start.valueChanged.connect(self.plot.setLowerMarker)
        self.slider_Stop.valueChanged.connect(self.plot.setUpperMarker)


        #
        # global layouts
        #
        grid_global = QtGui.QGridLayout()
        grid_global.addLayout(vBox_buttons        , 0, 0, 3, 1)
        grid_global.addWidget(groupBox_limits     , 0, 1, 1, 2)
        grid_global.addWidget(self.positionBox    , 1, 1)
        grid_global.addWidget(self.stepBox        , 1, 2)
        grid_global.addWidget(self.orientationBox , 2, 1, 1, 2)
        grid_global.addLayout(hBox_save           , 3, 0, 1, 3)
        grid_global.addWidget(self.table          , 4, 0, 1, 3)
        grid_global.addWidget(self.plot           , 5, 0, 1, 3)

        Measurement.setLayout(grid_global)
        Measurement.setWindowTitle('KnifeEdge - Measurement')
        Measurement.setGeometry(50,50,570,750)
        Measurement.show()


    def enableControls(self, avail):
        """Enable all control buttons except for the stop button

        Args:
            avail (bool): flag to enable/disable controls
        """
    
        for btn in [self.button_Run,
                    self.button_Preview,
                    self.button_Save,
                    self.button_Cancel,]:
            btn.setEnabled(avail)


    def passivMode(self, dataset):
        """Disable controls buttons and show the given dataset

        Args:
            dataset (RunData): instance of RunData to show
        """

        for btn in [self.button_Run,
                    self.button_Preview,
                    self.button_Stop,
                    self.button_Save,]:
            btn.setEnabled(False)

        self.label_filepath.setText(os.path.basename(dataset['filepath']))

        self.positionBox.value  = float(dataset['zpos'])

        self.slider_Start.value = float(dataset['destinations'][0])
        self.slider_Stop.value  = float(dataset['destinations'][-1])

        self.stepBox.stepNum  = len(dataset)
        self.stepBox.stepSize = \
                dataset['destinations'][1] - dataset['destinations'][0]

        self.orientationBox.orientation = dataset['orientation']
    
        self.plot.plot(dataset)
    
        self.table.model().datasets = [dataset]





class Measurement_Slider(QtGui.QWidget):
    """Custom slider widget for the Measurement control widget"""

    # Signal emitted when the value has changed
    valueChanged = QtCore.Signal(float)


    def __init__(self, label, default=0.0, range_=(0,1)):
        """Constructor

        Args:
            label (str): name of the slider printed as QLabel
            default (float): default value of slider and lineEdit
            range_ (tuple): pair of floats specifying the allowed range
        """

        super(Measurement_Slider, self).__init__()

        self._value = default 
        self.range_ = range_
        self.scale = 100 / (self.range_[1]-self.range_[0])

        self.label    = QtGui.QLabel(label)
        self.slider   = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.lineEdit = QtGui.QLineEdit()

        validator = QtGui.QDoubleValidator()
        validator.setRange(*range_)
        validator.setDecimals(4)

        self.lineEdit.setValidator(validator)
        self.lineEdit.editingFinished.connect(self.__on_editing_finished)

        self.slider.setRange(0,100)
        self.slider.sliderReleased.connect(self.__on_slider_released)

        self.valueChanged.connect(self.__update)
        self.__update()

        self.initUI()
    

    @property
    def value(self):
        """Get the current value

        Returns:
            float: current value
        """
        return self._value


    @value.setter
    def value(self, v):
        """Set the current value
        
        Args:
            v (float): the current value to set
        """
        self._value = v
        self.valueChanged.emit(v)


    def initUI(self):
        """Initialize the UI"""

        self.label.setFixedWidth(40)
        self.slider.setMinimumWidth(250)
        self.lineEdit.setFixedWidth(60)

        hBox = QtGui.QHBoxLayout()
        hBox.addWidget(self.label)
        hBox.addWidget(self.slider)
        hBox.addWidget(self.lineEdit)
        hBox.addWidget(QtGui.QLabel('mm'))
    
        self.setLayout(hBox)
    

    def __update(self, *args):
        """Set the value of the lineEdit widget
        """
        self.lineEdit.setText("%.4f" % self._value)
        self.slider.setValue( self._value * self.scale )
        
        
    def __on_editing_finished(self, *args):
        """Called when user finished editing the text box
        """
        if self.lineEdit.text() != "":
            self._value = float( self.lineEdit.text() )
            self.valueChanged.emit(self._value)

            
    def __on_slider_released(self, *args):
        """Called when user moved the slider
        """
        self._value = float( self.slider.value()/self.scale )
        self.valueChanged.emit(self._value)
        
        





class Measurement_StepBox(QtGui.QGroupBox):
    """GroupBox for setting the step size int the Measurement control widget"""
    
    def __init__(self, start, stop, default=1.0, range_=(0.0001,5.0)):
        """Constructor

        Args:
            start (float): lower bound position
            stop (float): upper bound position
            default (float): default values of stepSize
            range_ (tuple): allowed range of stepSize
        """
    
        super(Measurement_StepBox, self).__init__('Step')
    
        self.lEdit_stepSize = QtGui.QLineEdit()
        self.lEdit_stepNum  = QtGui.QLineEdit()

        validator = QtGui.QDoubleValidator()
        validator.setRange(*range_)
        validator.setDecimals(4)

        self.lEdit_stepSize.setValidator(validator)
        self.lEdit_stepSize.setText(str(default))
        self.lEdit_stepSize.editingFinished.connect(self.updateStepNum)

        self.lEdit_stepNum.setValidator(QtGui.QIntValidator())
        self.lEdit_stepNum.editingFinished.connect(self.updateStepSize)

        self.start = start
        self.stop  = stop
        self.updateStepNum()

        self.initUI()


    @property
    def stepSize(self):
        """Get step size
    
        Returns:
            float: step size from lineEdit
        """
        text = self.lEdit_stepSize.text()
        if text == "":
            text = 1.0
        return float(text)
    

    @stepSize.setter
    def stepSize(self, v):
        """Set the step size
        
        CAVEAT: This will not cause any update on stepNum or other widgets and
        thus may lead to inconsistent values.
        
        Args:
            v (float): stepSize to set
        """
        self.lEdit_stepSize.setText("%.4f" % v)


    @property
    def stepNum(self):
        """Get number of steps
    
        Returns:
            int: number of steps
        """
        text = self.lEdit_stepNum.text()
        if text == "":
            text = 0
        return int(text)
    

    @stepNum.setter
    def stepNum(self, v):
        """Set the number of steps 

        CAVEAT: This will not cause any update on stepSize or other widgets and
        thus may lead to inconsistent values.

        Args:
            v (int): number of steps to set
        """
        self.lEdit_stepNum.setText("%d" % v)
    

    def initUI(self):
        """Initialize the UI"""

        self.lEdit_stepNum.setFixedWidth(60)
        self.lEdit_stepSize.setFixedWidth(60)

        hBox_step = QtGui.QHBoxLayout()
        hBox_step.addStretch(1)
        hBox_step.addWidget(self.lEdit_stepSize)
        hBox_step.addWidget(QtGui.QLabel('mm'))
        hBox_step.addStretch(1)
        hBox_step.addWidget(self.lEdit_stepNum)
        hBox_step.addWidget(QtGui.QLabel('points'))
        hBox_step.addStretch(1)
        self.setLayout(hBox_step)


    def updateStepNum(self):
        """Calculate the number of steps from start and stop value given the
        step size
        """
        steps = int( (self.stop - self.start) / self.stepSize ) + 1
        self.lEdit_stepNum.setText(str(steps))


    def updateStepSize(self):
        """Calculate the stepsize from start and stop value given the number of
        steps
        """
        stepSize = (self.stop - self.start) / (self.stepNum-1)
        self.lEdit_stepSize.setText("%.4f" % stepSize)
        

    @QtCore.Slot(float)
    def stopValueChanged(self, value):
        """Call method to update the stepNum LineEdit"""
        self.stop = value
        self.updateStepSize()


    @QtCore.Slot(float)
    def startValueChanged(self, value):
        """Call method to update the stepNum LineEdit"""
        self.start = value
        self.updateStepSize()





class Measurement_PositionBox(QtGui.QGroupBox):
    """GroupBox for setting the z-Position in the Measurement control widget"""
    
    def __init__(self, default=0.0, range_=(0.0,50.0)):
        """Constructor
    
        Args:
            default (float): default value written in the box
            range_ (tuple): pair of floats specifying the absolute range
        """
    
        super(Measurement_PositionBox, self).__init__('z-Position')
    
        self.lEdit = QtGui.QLineEdit()
    
        self.lEdit.setValidator(QtGui.QDoubleValidator(0.0,50.0,4))
        self.lEdit.setText('0.0')
    
        self.initUI()


    @property
    def value(self):
        """Get the value
    
        Returns:
            float: z-Position from the lineEdit
        """
        text = self.lEdit.text()
        if text == "":
            text = 0.0
        return float(text)


    @value.setter
    def value(self, v):
        """Set the value
    
        Args:
            v (float): value to set
        """
        self.lEdit.setText("%.4f" % v)


    def initUI(self):
        """Initialize the UI"""

        self.lEdit.setFixedWidth(60)
        hBox_position = QtGui.QHBoxLayout()
        hBox_position.addStretch(1)
        hBox_position.addWidget(self.lEdit)
        hBox_position.addWidget(QtGui.QLabel('mm'))
        self.setFixedWidth(150)
        self.setLayout(hBox_position)





class Measurement_OrientationBox(QtGui.QGroupBox):
    """GroupBox for setting the orientation in the Measurement control widget"""
    
    def __init__(self):
        """Constructor"""

        super(Measurement_OrientationBox, self).__init__('Orientation')
    
        self.radio_Horizontal = QtGui.QRadioButton('Horizontal', self)
        self.radio_Vertical   = QtGui.QRadioButton('Vertical', self)

        self.radio_Horizontal.setChecked(True)
    
        self.initUI()


    def initUI(self):
        """Initialize the UI"""

        hBox_orientation = QtGui.QHBoxLayout()
        hBox_orientation.addStretch(1)
        hBox_orientation.addWidget(self.radio_Horizontal)
        hBox_orientation.addStretch(1)
        hBox_orientation.addWidget(self.radio_Vertical)
        hBox_orientation.addStretch(1)
        self.setLayout(hBox_orientation)
    

    @property
    def isHorizontal(self):
        """Flag for horizontal orientation
    
        Returns:
            bool: True when orientation is horizontal
        """
        return self.radio_Horizontal.isChecked()
    

    @property
    def isVertical(self):
        """Flag for vertical orientation
    
        Returns:
            bool: True when orientation is vertical
        """
        return self.radio_Vertical.isChecked()


    @property
    def orientation(self):
        """Get the orientation as string abbreviation
    
        Returns:
            str: orientation as string, either 'H' or 'V'
        """

        if self.isHorizontal:
            return 'H'
        else:
            return 'V'


    @orientation.setter
    def orientation(self, o):
        """Set the orientation
        
        Args:
            o (str): Must be either 'H' or 'V'
        """

        if o == 'H':
            self.radio_Horizontal.setChecked(True)
            self.radio_Vertical.setChecked(False)
        elif o == 'V':
            self.radio_Horizontal.setChecked(False)
            self.radio_Vertical.setChecked(True)
        else:
            self.radio_Horizontal.setChecked(False)
            self.radio_Vertical.setChecked(False)
