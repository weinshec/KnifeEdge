"""table.py - GUI classes for showing RunData in table view
"""

from PySide import QtCore, QtGui

from ..dataset import RunData



class RunDataTableModel(QtCore.QAbstractTableModel):
    """GUI classes for showing RunData in table view"""

    def __init__(self, keys, datasets=[]):
        """Constructor

        Args:
            keys (list): list of keys in RunData to assign a column for
            datasets (list): list of RunData instances to show on the table
        """
    
        super(RunDataTableModel, self).__init__()

        self._datasets = datasets
        
        self.keys = keys


    @property
    def datasets(self):
        """Get the datasets
    
        Returns:
            list: list of RunData
        """
        return self._datasets


    @datasets.setter
    def datasets(self, datasets):
        """Set datasets
    
        Args:
            datasets (list): new list of RunData
        """
        self._datasets = datasets
        self.modelReset.emit()     # signal the TableView to update


    def rowCount(self, *args):
        """Overwrite: number of rows is equal to number of RunData in list

        Returns:
            int: number of rows
        """
        return len(self._datasets)


    def columnCount(self, *args):
        """Overwrite: number of columns equal to length of keys in list

        Returns:
            int: number of columns
        """
        return len(self.keys)


    def data(self, index, role):
        """Overwrite: called when setting the contents of a cell

        Args:
            index (QModelIndex): index of the data cell
            role (int): display role of data cell

        Returns:
            object: requested cell content
        """
        dset = self._datasets[index.row()]
        key  = self.keys[index.column()]
        val  = dset[ key ]

        if role == QtCore.Qt.DisplayRole:
            if val is None or val == []:
                return '-'
            if type(val) == float:
                return "%.4f" % val
            return str( val )
    
        elif role == QtCore.Qt.TextAlignmentRole:
            return QtCore.Qt.AlignCenter


    def headerData(self, section, orientation, role):
        """Overwrite: called when setting the header labels

        Args:
            section (int): index of the col/row
            orientation (Qt.Orientation): choose horizontal or vertial
            role (int): display role of header cell

        Returns:
            object: requested header cell content
        """

        default_widths = {'width'       : 175,
                          'width9010'   : 150,
                          'orientation' : 75,}

        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                return self.keys[section]
            if orientation == QtCore.Qt.Vertical:
                return section + 1
    
        elif role == QtCore.Qt.SizeHintRole:
            if orientation == QtCore.Qt.Horizontal:
                width = default_widths.get( self.keys[section], 100)
                return QtCore.QSize(width,30)





class RunDataTableView(QtGui.QTableView):
    """GUI class extending QTableView for showing RunData in a DataTabelModel
    """

    # emitted when user double clicked on a table row
    doubleClicked_dataset = QtCore.Signal(RunData)

    # emitted when user pressed del key on a table row
    deletePressed = QtCore.Signal(int)


    def __init__(self, model):
        """Constructor

        Args:
            model (RunDataTableModel): model to show on this table
        """
    
        super(RunDataTableView, self).__init__()
    
        self.setModel(model)

        self.doubleClicked.connect(self.on_doubleClicked)
    
        self.verticalScrollBar().rangeChanged.connect(self.resizeScroll)


    def on_doubleClicked(self, index):
        """Emit the doubleClicked_dataset signal with the corresponing RunData

        Args:
            index (QModelIndex): index of table element clicked on
        """
        self.doubleClicked_dataset.emit( self.model().datasets[index.row()] )


    def keyPressEvent(self, e):
        """Emit the deletePressed signal with the corresponding row

        Args:
            e (Event): event instance of keyPress event
        """

        if e.key() == QtCore.Qt.Key_Delete:
            row = self.currentIndex().row() 
            if row != -1:
                self.deletePressed.emit( row )


    def setModel(self, *args):
        """Overwrite setModel method to autoresize contents when a new model is
        applied
        """
        super(RunDataTableView, self).setModel(*args)
        self.resizeColumnsToContents()
        self.resizeRowsToContents()


    def resizeScroll(self, min_, max_):
        """Implements autoscrolling of the table"""

        self.verticalScrollBar().setValue(max_)
