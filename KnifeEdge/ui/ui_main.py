"""ui_main.py - GUI definitions for the Main class
"""

from PySide import QtGui, QtCore

from .plot import WaistPlot
from .table import RunDataTableModel, RunDataTableView



class UI_Main(object):
    """GUI definitions for the Main class"""

    def initUI(self, Main):
        """Initialize the UI"""

        self.parent = Main

        #
        # Button Box
        #
        self.button_NewRun     = QtGui.QPushButton('New Run'     , Main)
        self.button_NoiseRun   = QtGui.QPushButton('Noise Run'   , Main)
        self.button_SaveWidths = QtGui.QPushButton('Save Widths' , Main)
        self.button_FitWaist   = QtGui.QPushButton('Fit Waist'   , Main)
        self.button_SaveFig    = QtGui.QPushButton('Save Fig'    , Main)
        self.button_Quit       = QtGui.QPushButton('Quit'        , Main)

        vBox_buttons = QtGui.QVBoxLayout()
        vBox_buttons.setSpacing(10)
        vBox_buttons.addWidget(self.button_NewRun)
        vBox_buttons.addWidget(self.button_NoiseRun)
        vBox_buttons.addWidget(self.button_SaveWidths)
        vBox_buttons.addStretch(1)
        vBox_buttons.addWidget(self.button_FitWaist)
        vBox_buttons.addWidget(self.button_SaveFig)
        vBox_buttons.addStretch(1)
        vBox_buttons.addWidget(self.button_Quit)
        self.button_NewRun.setFixedSize(100,75)
        for btn in [self.button_NoiseRun,
                    self.button_SaveWidths,
                    self.button_FitWaist,
                    self.button_SaveFig,
                    self.button_Quit]:
            btn.setFixedSize(100,30)

        self.button_NewRun.clicked.connect(Main.newRun)
        self.button_NoiseRun.clicked.connect(Main.noiseRun)
        self.button_SaveWidths.clicked.connect(Main.saveWidths)
        self.button_SaveWidths.setEnabled(False)
        self.button_FitWaist.clicked.connect(Main.fitWaist)
        self.button_FitWaist.setEnabled(False)
        self.button_SaveFig.clicked.connect(Main.saveFig)
        self.button_SaveFig.setEnabled(False)
        self.button_Quit.clicked.connect(Main.close)


        #
        # Plot Widget
        #
        self.plot = WaistPlot()


        #
        # Table Widget
        #
        model = RunDataTableModel(keys=['orientation','zpos','width','width9010'])

        self.table = RunDataTableView(model)
        self.table.doubleClicked_dataset.connect(Main.showRunData)
        self.table.deletePressed.connect(Main.delMeasurement)


        #
        # global layouts
        #
        grid_global = QtGui.QGridLayout()
        grid_global.addLayout(vBox_buttons, 0, 0)
        grid_global.addWidget(self.plot   , 0, 1)
        grid_global.addWidget(self.table  , 1, 0, 1, 2)

        Main.setLayout(grid_global)
        Main.setWindowTitle('KnifeEdge - Main')
        Main.setGeometry(50,50,570,700)
        Main.show()



