"""ui_noise.py - GUI definitions for the Noise class
"""

import os
import sys
from PySide import QtGui, QtCore
from pkg_resources import Requirement, resource_filename

from .plot import NoiseDataPlot



class UI_Noise(object):
    """GUI definitions for the Measurement class"""

    def initUI(self, Noise):
        """Initialize the UI"""

        self.parent = Noise

        #
        # Button Box
        #
        self.button_Run     = QtGui.QPushButton('Start'     , Noise)
        self.button_Stop    = QtGui.QPushButton('Stop'      , Noise)

        hBox_buttons = QtGui.QHBoxLayout()
        vBox_buttons = QtGui.QVBoxLayout()
        vBox_buttons.setContentsMargins(10,10,10,10)
        vBox_buttons.setSpacing(10)
        vBox_buttons.addWidget(self.button_Run)
        vBox_buttons.addWidget(self.button_Stop)
        self.button_Run.setFixedSize(100,100)
        self.button_Stop.setFixedSize(100,40)
        hBox_buttons.addStretch(1)
        hBox_buttons.addLayout(vBox_buttons)
        hBox_buttons.addStretch(1)

        self.button_Run.clicked.connect(Noise.run)
        self.button_Stop.clicked.connect(Noise.stop)


        #
        # Options Box
        #
        self.options = Noise_OptionsBox()
        self.options.check_Saturation.stateChanged.connect(Noise.setSaturation)
        

        #
        # Save File Box
        #
        self.button_Save    = QtGui.QPushButton('', Noise)
        self.button_Cancel  = QtGui.QPushButton('', Noise)
        self.label_filepath = QtGui.QLabel(Noise.filepath)

        save_icon = resource_filename(
            Requirement.parse("KnifeEdge"),
            "KnifeEdge/ui/icons/Button-Ok-icon.png"
        )
        cancel_icon = resource_filename(
            Requirement.parse("KnifeEdge"),
            "KnifeEdge/ui/icons/Button-Delete-icon.png"
        )
        
        self.button_Save.setIcon(QtGui.QIcon(save_icon))
        self.button_Cancel.setIcon(QtGui.QIcon(cancel_icon))
        hBox_save = QtGui.QHBoxLayout()
        hBox_save.setContentsMargins(10,10,10,10)
        hBox_save.addSpacing(15)
        hBox_save.addWidget(self.button_Cancel)
        hBox_save.addSpacing(10)
        hBox_save.addWidget(self.button_Save)
        hBox_save.addSpacing(30)
        hBox_save.addWidget(self.label_filepath)
        self.button_Save.setFixedSize(25,25)
        self.button_Cancel.setFixedSize(25,25)

        self.button_Cancel.clicked.connect(Noise.close)
        self.button_Save.clicked.connect(Noise.save)


        #
        # Histogram Widget
        #
        self.histogram = NoiseDataPlot()


        #
        # Plot Widget
        #
        self.plot = NoiseDataPlot()


        #
        # global layouts
        #
        grid_global = QtGui.QGridLayout()
        grid_global.addLayout(hBox_buttons        , 0, 0)
        grid_global.setRowStretch(1,1)
        grid_global.addWidget(self.options        , 2, 0)
        grid_global.addWidget(self.histogram      , 0, 1, 3, 1)
        grid_global.addLayout(hBox_save           , 3, 0, 1, 2)
        grid_global.addWidget(self.plot           , 4, 0, 1, 2)

        Noise.setLayout(grid_global)
        Noise.setWindowTitle('KnifeEdge - Noise')
        Noise.setGeometry(50,50,570,750)
        Noise.show()


    def enableControls(self, avail):
        """Enable all control buttons except for the stop button
        
        Args:
            avail (bool): flag to enable/disable controls
        """
        
        for btn in [self.button_Run,
                    self.button_Save,
                    self.button_Cancel,]:
            btn.setEnabled(avail)





class Noise_OptionsBox(QtGui.QGroupBox):
    """GroupBox for setting options in the Noise control widget"""


    def __init__(self):
        """Constructor"""
    
        super(Noise_OptionsBox, self).__init__('Options')
    
        self.check_Saturation = QtGui.QCheckBox('Show Saturation', self)
        self.check_NumPoints  = QtGui.QCheckBox('Stop after'     , self)
        self.lEdit_NumPoints  = QtGui.QLineEdit()
    
        self.lEdit_NumPoints.setValidator(QtGui.QIntValidator())
        self.lEdit_NumPoints.setText('0')
    
        self.initUI()


    @property
    def numPoints(self):
        """Get the value
        
        Returns:
            float: number of points
        """
        text = self.lEdit_NumPoints.text()
        if text == "":
            text = 0
        return int(text)


    @numPoints.setter
    def numPoints(self, v):
        """Set the value
        
        Args:
            v (int): value to set
        """
        self.lEdit_NumPoints.setText("%d" % int(v))


    def initUI(self):
        """Initialize the UI"""

        self.lEdit_NumPoints.setFixedWidth(60)

        vBox = QtGui.QVBoxLayout()
        vBox.addWidget(self.check_Saturation)
        vBox.addStretch(1)
        vBox.addWidget(self.check_NumPoints)
        hBox_numPoints = QtGui.QHBoxLayout()
        hBox_numPoints.addStretch(1)
        hBox_numPoints.addWidget(self.lEdit_NumPoints)
        hBox_numPoints.addWidget(QtGui.QLabel('pts.'))
        vBox.addLayout(hBox_numPoints)

        self.setFixedWidth(150)
        self.setLayout(vBox)

