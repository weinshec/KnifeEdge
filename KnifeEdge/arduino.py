"""arduino.py - Controller of the Arduino Uno used as ADC
"""

import serial
import time


class Arduino(object):
    """Controller class for the Arduino Uno used as ADC"""


    def __init__(self, name, port='/dev/ttyACM0'):
        """Constructor

        Args:
            name (string): internal name of this device
            port (string): unix device path the PM100D is attached to
        """

        # Call the superconstructor
        super(Arduino, self).__init__()

        self._pm = serial.Serial(port     = port,
                                 baudrate = 9600,
                                 timeout  = 1)

        time.sleep(2) # timeout waiting for Arduino to reset



    def read(self):
        """Call the device routine for reading a value"""

        if not self._pm:
            raise RuntimeError('Arduino not initialized')

        self._pm.write(b'r') # send read command
        reply = self._pm.readline().decode('utf-8')
        val = int(reply[:-2])
        return val
