"""measurement.py - QtWidget performing the actual measurement run
"""

from PySide import QtGui, QtCore
import numpy as np
import os
from multiprocessing import Lock

from pyAPT import ControllerStatus

from .ui.ui_measurement import UI_Measurement
from .runner import Runner
from .listener import RunnerListener
from .dataset import RunData



class Measurement(QtGui.QWidget):
    """QtWidget performing the actual measurement run"""


    saved = QtCore.Signal(RunData)  # emitted when the dataset has been saved


    def __init__(self, dataset=None):
        """Constructor

        Calling this constructor with ``dataset`` argument will disable all
        controls. This is for showing old datasets only.

        Args:
            dataset (RunData): instance of RunData to show in this widget
        """

        super(Measurement, self).__init__()

        self.config    = QtCore.QCoreApplication.instance().config
        self.fileIO    = QtCore.QCoreApplication.instance().fileIO
        self.runner    = None
        self.zRunner   = None
        self.listener  = None
        self.zListener = None
        self.data      = None
        self.seq_lock  = Lock()

        self.ui = UI_Measurement()
        self.ui.initUI(self)

        if dataset:
            self.data = dataset
            self.ui.passivMode(dataset)


    def profile(self, dests):
        """Start a profiling measurement

        Start a profiling measurment given a list of desitnations. This will
        start a Runner and Listener instance and initialize an empty dataset.

        Args:
            dests (list): list of destinations for profiling
        """

        orientation = self.ui.orientationBox.orientation

        if self.config.getboolean('stage'+orientation, 'active') == False:
            info = QtGui.QMessageBox.warning(self, 'Warning',
                    'stage%s is deactivated in config file' % orientation)
            return

        if self.config.getboolean('general','demo'):
            self.data = RunData.fake(num    = len(dests),
                                     range_ = (dests[0],dests[-1]),
                                     noise  = 10.0,)
        else:
            self.data = RunData.empty()

        self.data['orientation']  = orientation
        self.data['destinations'] = dests

        if self.config.getboolean('general','demo'):
            self.notify_measurementEnd()
            return

        self.ui.enableControls(False)

        if self.config.getboolean('stageZ', 'active') == True:
            self.zRunner   = Runner(self.config, 'stageZ',
                                    lock=self.seq_lock)
            self.zRunner.start()

            self.zListener = RunnerListener(self.zRunner)
            self.zListener.notify.connect(self.notify_zPos)
            self.zListener.start()

        self.runner   = Runner(self.config, 'stage'+orientation,
                               lock=self.seq_lock)
        self.runner.start()

        self.listener = RunnerListener(self.runner)
        self.listener.notify.connect(self.notify)
        self.listener.terminated.connect(self.notify_measurementEnd)
        self.listener.start()

        if self.config.getboolean('stageZ', 'active') == True:
            self.zRunner.con.send(float(self.ui.positionBox.value))
            self.zRunner.con.send('q')
        else:
            self.data['zpos'] = float(self.ui.positionBox.value)

        for dest in dests:
            self.runner.con.send(float(dest))
            self.runner.con.send('r')
        self.runner.con.send('q')


    def run(self):
        """Perform a run with specified range and steps settings"""

        destinations = np.linspace(self.ui.slider_Start.value,
                                   self.ui.slider_Stop.value,
                                   self.ui.stepBox.stepNum)
        self.profile(destinations)


    def preview(self):
        """Perform a preview run with grain steps"""

        self.profile( np.linspace(0.0,25.0,26) )


    def stop(self):
        """Stop the Runner process"""

        if self.listener:
            self.listener.terminated.disconnect(self.notify_measurementEnd)

        if self.runner:
            self.runner.term()

        self.ui.enableControls(True)


    def save(self):
        """Save the dataset and dismiss the Measurement widget"""

        # No data, do nothing
        if self.data is None:
            return
        else:
            if self.config.getboolean('stageZ', 'active') == False:
                self.data['zpos'] = float(self.ui.positionBox.value)

            filebasename = self.fileIO.filepath() + "_%s%.4f" \
                    % (self.data['orientation'], self.data['zpos'])

            self.data   .saveToFile( filebasename + ".csv" )
            self.ui.plot.saveToFile( filebasename + ".pdf" )
            self.data['filepath'] = filebasename

            self.saved.emit(self.data)
            self.close()


    def closeEvent(self, event):
        """Overwrite behavior on closing the window"""

        print('Measurment close event')

        # There is no data
        if self.data is None:
            event.accept()

        # There is unsaved data
        elif 'filepath' not in self.data:
            reply = QtGui.QMessageBox.question(self, 'Message',
                        "Really discard this measurment",
                        QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                        QtGui.QMessageBox.No)
            if reply == QtGui.QMessageBox.Yes:
                event.accept()
            else:
                event.ignore()

        # Save is save
        else:
            event.accept()


    def set_range_auto(self):
        """Set range parameters automatically after a preview run
        """

        if 'pout' not in self.data:
            return

        z_c = self.data['pout'][2]
        w   = self.data['pout'][3]

        xmin = max( 0.0, z_c - 2.0*w)
        xmax = min(25.0, z_c + 2.0*w)

        self.ui.slider_Start.value = xmin
        self.ui.slider_Stop.value  = xmax
        self.ui.stepBox.stepNum    = 100
        self.ui.plot.setLowerMarker(xmin)
        self.ui.plot.setUpperMarker(xmax)


    def notify(self, res):
        """Notify method to call when a new result is ready

        Args:
            res (object): the result from the device command executed by runner
        """

        if type(res) == ControllerStatus:
            self.data['position'].append(res.position)
        else:
            self.data['intensity'].append(res)
            self.ui.plot.plot(self.data)


    def notify_zPos(self, res):
        """Notify method to call when the z-position should be updated

        Args:
            res (ControllerStatus): controller status instance of zStage
        """

        self.data['zpos'] = res.position


    def notify_measurementEnd(self):
        """Notify method to call when the measurement ended."""

        self.ui.enableControls(True)
        self.data.addWidthFit()
        try:
            self.data.addWidth9010()
        except (IndexError):
            print("Could not estimate width9010")
        self.ui.table.model().datasets = [self.data]
        self.ui.plot.plot(self.data)
        self.set_range_auto()


    @property
    def filepath(self):
        """Filepath of the this runs dataset"""

        if self.data is None:
            return "-"
        elif 'filepath' in self.data:
            return os.path.basename(self.data['filepath'])
