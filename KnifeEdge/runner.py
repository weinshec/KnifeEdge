"""runner.py - multithreaded class to perform physical actions asynchronously
"""

import time
import multiprocessing
from multiprocessing.managers import BaseManager

from pyAPT import Z825B, MTS50, ControllerStatus
from .arduino import Arduino



class Runner(multiprocessing.Process):
    """Runner class to perform asynchronous device control actions.

    The Runner object initializes a communication pipe (``multithreading.Pipe``)
    to communicate with the parent process. Commands have to be sent to the
    parents end of the pipe, i.e. ``Runner.con`` before starting the actual
    Runner thread. Command results may then be recieved by the parent process
    from the same connection (duplex-pipe).

    For a list of possible commands, see ``Runner.run()``
    
    Additionally when initializing with a multiprocessing.Lock parameters this
    process acquires the lock when started and releases it, when ending.
    """

    def __init__(self, config, stage, noread=False, lock=None):
        """Constructor

        Args:
            config (Config): Config instance holding device options
            stage (str): Name of the stage to move (same as in config file).
                Pass 'None' for read-only runner
            noread (bool): if True, no reading will be available
            lock (multiprocessing.Lock): lock being acquired when process is
                running
        """

        super(Runner, self).__init__()

        # The communication pipe with the parent process.
        self.con, self._con = multiprocessing.Pipe()

        # Process lock being acquired when process starts and released
        # when it ends
        self._lock = lock

        # Register device proxies. This is nessecary to instantiate device
        # object in shared memory so that worker processes may access them.
        manager = BaseManager()
        manager.register('Z825B',   Z825B)
        manager.register('MTS50',   MTS50)
        manager.register('Arduino', Arduino)
        manager.start()


        if stage:
            if not config.getboolean(stage, 'active'):
                raise RuntimeError('Trying to move non-active stage %s' % stage)

            model  = config.get(stage,'model')
            serial = config.get(stage,'serial')
            scale  = config.getfloat(stage,'scale')
            if model == "Z825B":
                self.stage = manager.Z825B(serial, scale_correction=scale)
            elif model == "MTS50":
                self.stage = manager.MTS50(serial, scale_correction=scale)
            else:
                raise RuntimeError('Stage model not supported %s' % model)
            
            _,_,self.vmax  = self.stage.velocity_parameters()
            
        else:
            self.stage = None

        if noread:
            self.arduino = None
        else:
            self.arduino = manager.Arduino(name   = "Arduino",
                                           port   = config.get('arduino','port'))


    def run(self):
        """Execution loop for commands.

        This loop will receives and executes command from the communication pipe
        until it is empty.

        List of possible commands:
            + 'q'   : terminate this runner
            + 'r'   : read value from arduino
            + 'cr'  : read in 0.1s intervals from arduino
            + float : move stage to `float` position
            + 'h'   : home the stage
            + 's'   : request controller status
        """

        if self._lock:
            print('Runner pending...')
            self._lock.acquire()

        print('Runner started...')

        with multiprocessing.Pool(processes=1) as pool:

            while True:
                
                cmd = self._con.recv()

                # Move command
                if type(cmd) in [int,float] and self.stage:
                    
                    delta = abs(self.stage.position() - cmd)
                    tmax  = max(5.0, int(2.0 * delta / self.vmax))
                    
                    res = pool.apply_async(self.stage.goto, (cmd,),
                            callback=self.done,
                            error_callback=self.err)
                    res.wait(tmax)

                # Read command
                elif cmd == 'r' and self.arduino:
                    res = pool.apply_async(self.arduino.read,
                            callback=self.done,
                            error_callback=self.err)
                    res.wait(1)

                # Continuous read command
                elif cmd == 'cr' and self.arduino:
                    time.sleep(0.1)
                    res = pool.apply_async(self.arduino.read,
                            callback=self.done,
                            error_callback=self.err)
                    res.wait(1)
                    self.con.send('cr')

                # Home command
                elif cmd == 'h' and self.stage:
                    res = pool.apply_async(self.stage.home,
                            callback=self.done,
                            error_callback=self.err)
                    res.wait(120)

                # Request controller status
                elif cmd == 's' and self.stage:
                    res = pool.apply_async(self.stage.status,
                            callback=self.done,
                            error_callback=self.err)

                elif cmd == 'q':
                    print('Recieved q command')
                    break

                else:
                    print('Command not recognized!')

        
        if self._lock:
            self._lock.release()
            
        if self.stage:
            try:
                self.stage.close()
            except Exception as e:
                print(e)
                
        print('Runner terminated')


    def done(self, result):
        """Callback method for asynchronous calls

        Args:
            result (object): resulting object from async call
        """

        self._con.send(result)


    def err(self,e):
        """Error handling for failed async calls"""

        print('Error from async call:\t',e)


    def term(self):
        """Priority termination

        Clear the command queue and send quit command
        """

        while self._con.poll():
            self._con.recv()
        self.con.send('q')

