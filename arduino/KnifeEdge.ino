/*
    KnifeEdge.ino

    Open a serial port and listen for READ commands. When issued, read the
    analog port 3 and output its ADC counts.

    ADC set to use internal 1.1V source as reference due to better stability.
    Use according voltage divider circuit.

    Author: Christoph Weinsheimer
    Email : christoph.weinsheimer@desy.de
*/


int analogPin = 0;      // analog input pin
int statusLED = 13;     // internal LED
int value     = -1;

char READ       = 'r';
bool flushValue = false;



void setup() {
    // select internal 1.1 volt reference
    analogReference(INTERNAL);

    // Start serial port at 9600 baud
    Serial.begin(9600);

    // set led to output for showing status
    pinMode(statusLED, OUTPUT);
}



void loop() {
    if (flushValue) {
        Serial.println(value);
        value      = -1;
        flushValue = false;
    }
}

void serialEvent() {
    while (Serial.available()) {
        // get the new byte:
        char inChar = (char)Serial.read();

        if (inChar == READ) {
            value = analogRead(analogPin);
            flushValue = true;
        }
    }
}
