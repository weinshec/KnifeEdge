#!/usr/bin/env python
# encoding: utf-8

from pathlib import Path
from setuptools import setup

with Path("./README.rst").open(encoding="utf-8") as f:
    long_description = f.read()

with Path("./requirements.txt").open(encoding="utf-8") as f:
    requirements = list(filter(None, (row.strip() for row in f)))

info = {
    "name":                "KnifeEdge",
    "version":             "2.0.1",
    "author":              "Christoph Weinsheimer",
    "author_email":        "christoph.weinsheimer@desy.de",
    "url":                 "https://gitlab.com/weinshec/KnifeEdge",
    "packages":            ["KnifeEdge"],
    "provides":            ["KnifeEdge"],
    "description":         "Knife Edge beam waist measurement tool",
    "long_description":    long_description,
    "install_requires":    requirements,
    "package_data":        {'': ['KnifeEdge.conf'], 'KnifeEdge.ui': ['icons/*.png']},
    "entry_points":        {'gui_scripts': ['kestart = KnifeEdge.core:start_gui'],},
}

if __name__ == "__main__":
    setup(**info)
