Introduction
============
.. _introduction:

This page will introduce you to the concept of Knife Edge measurements in
theory. If you like to learn how to operate the Knife Edge Unit, see chapter
:ref:`Operation <tutorial>`.


Little theory of Gaussian beams
-------------------------------
.. _gaussian_beams:

This section gives only a rough overview on Gaussian beams and therefor is not
intended to work a general introduction. For a more sophisticated introduction
to Gaussian optics refer to e.g. [MES06]_.

Gaussian beams in fundamental mode propagating along the z-axis may be
represented as

.. math::
    E(z,\rho) = \frac{A_0}{kq(z)}
                \exp{\left( i \frac{k\rho^2}{2q(z)} \right)}
                e^{ikz}

with the *complex beam parameter* :math:`q(z)` conventionally written as

.. math::
    \frac{1}{q(z)} = \frac{1}{R(z)} + i \frac{2}{kw^2(z)}

This parameters fully characterizes the Gaussian beam in the :math:`TEM_{00}`
mode. For the scope of this article, the most important quantity here is the
**beam radius** :math:`w(z)` which can be written as

.. math::
    w^2(z) = w_0^2 \left( 1 + \left(\frac{z}{z_0}\right)^2 \right)

The minimum of this function at :math:`z=0` is called the **beam waist**
:math:`w_0^2 = \lambda z_0 / \pi` with :math:`\lambda` being the wavelength of
the laser light and :math:`z_0` being the so called *Rayleigh range*
corresponding to the distance from the beam waist, where the beam radius has
increased by a factor of :math:`\sqrt{2}`.

The physically observable quantity however is the beams **intensity** profile,
which can be derived by by multiplying the electrical field with its complex
conjugate:

.. math::
    I(z,\rho) \propto EE^{*}
        = |A_0| \left( \frac{w_0}{w(z)} \right)^2 e^{-2(\rho/w(z))^2}

For a given value of :math:`z` the beam radius :math:`w(z)` now represents the
distance from the beam center where the intensity dropped to :math:`1/e^2`,
i.e. 13% of its maximum. This in turn means that 87% if the beams power is
concentrated within a radius of :math:`w(z)` around the beam center.

.. figure:: img/waist.png
    :align: center

    Beam radius (red) shown in units of the Rayleigh length


The KnifeEdge technique
-----------------------
.. _technique:

The KnifeEdge technique (also referred to as *scanning knife edge*) uses the
sharp edge i.e. the knife, translated perpendicular through the beam while
recording the remaining power not blocked by the knife dependent on its
position. Mathematically this corresponds to integrating the 2D Gaussian beam
profile projected on the knifes translation axis.

Assume w.l.o.g. the knife being translated along the x-axis through the beam at
a given position :math:`z`. The power still passing the knife dependent on its
position is then given by

.. math::
    \begin{align*}
    P(x) &= \int_{-\infty}^{x} \int_{-\infty}^{\infty} I(x',y) \; dx'dy
            \\[1em]
         &= I_0 \frac{\pi}{4} w_x w_y
            \left( 1 + \text{erf} \left[ \frac{\sqrt{2}x}{w_x} \right] \right)
            \\[1em]
         &= \frac{P_0}{2}
            \left( 1 + \text{erf} \left[ \frac{\sqrt{2}x}{w_x} \right] \right)
    \end{align*}

where :math:`w_x` refers to the beam width in x-direction at this z-position
and :math:`P_0` being the total emitted power which - appearing as global
scaling constant - is not important for the beam width measurement


Estimating the beam width
-------------------------
.. _width_estimation:

The estimation of the beam width :math:`w(z)` can be done in different ways.
The following sections will describe the methods currently implemented in the
application.

Profile Fitting
~~~~~~~~~~~~~~~
.. _profile:

The most straight forward method of estimating the beam width at a given
z-position is recording the full power profile as described in the
:ref:`previous chapter <technique>` and fitting it with the following function:

.. math::
    f(x; c, P, \mu, w_x) = c + \frac{P}{2}
           \left(
               1 + \text{erf} \left[ \frac{\sqrt{2}(x-\mu)}{w_x} \right]
           \right)

This yields the beam width :math:`w_x` as fit parameter together with the
position of the beam center :math:`\mu` relative to the knifes origin position.
However, this method requires the beam to be close to Gaussian. For beams with
high contribution of second or higher order mode contribution, this may lead to
misleading width estimates.

90%-10%-Clipping
~~~~~~~~~~~~~~~~
.. _clipping:

This method is supposed tackle the shortcomings of the :ref:`Profile Fitting
<profile>` method when estimating the width of laser beams with significant
content of higher order modes. The idea is to determine the x-positions in the
power profile :math:`P(x)`, where takes 10% and 90% of the total power. The
absolute distance between those two points then equals to

.. math::
    |x_{90\%} - x_{10\%}| = 1.28 w_x

as one can easily proof using standard probability tables. The advantage of
this method is now, that using these 10%-90% clipping levels is robust against
contamination of the first higher order modes, as shown in [SIE91]_.


References
----------
.. [MES06] Meschede, Dieter: Optics, light and lasers; Wiley, 2006
.. [SIE91] A.E.Siegman et al.: Choice of clip levels for beam width
           measurements using knife-edge techniques; IEEE Xplore, 1991
