Operation
=========
.. _tutorial:

This page will teach you, how to start and operate the Knife Edge Unit
performing beam width measurements. We will start with setting up the physical
device and continue with the interaction of the user interface. At the end of
this section, you will find the trouble shooting section.


Setting up the device
---------------------
.. _setup_device:

The Knife Edge Unit comes with three motorized stages (one for each dimension
in space), a photodiode for the power measurement and of course a set of
knifes. In the following, we will go through all these components describing
in detail how to use them.

The motorized stages
~~~~~~~~~~~~~~~~~~~~
.. _stages:

There two types of **motorized stages**, the MTS50_ allowing for 50mm
translation meant to be used for the longitudinal displacement (z-direction),
and two Z825B_ stages for the transversal movement (x-, y-direction) covering
25mm travel range each.

The transversal stages are mounted perpendicular to each other, stacked on the
right-angle bracket as shown in the picture below. While mounting/dismounting
keep an eye on the small studs in between the stages, that assure the
orthogonality.

.. figure:: img/transversal_stages.jpg
    :align: center
    :width: 50%
    
    Transversal stages stacked on an right angle bracket perpendicular to each
    other

To mount the right-angle bracket carrying the transversal stages on the
longitudinal stage, you need to fix the aluminum adapter plate in between them
(see picture below)

.. figure:: img/adapter_plate.jpg
    :align: center
    :width: 50%

    Using the aluminum adapter plate to mount the right angle bracket on the
    longitudinal stage

The **pair of knifes** is glued together enclosing a right angle between them.
They are mounted on the foremost transversal stage using the angle plate fixed
to an arbitrary position on the stage. Make sure that the knife edges are
correctly aligned with respect to the ground, i.e. vertical and horizontal.

.. figure:: img/knifes.jpg
    :align: center
    :width: 50%

    Pair of knifes attached to the foremost stage using the angle plate

When placing the unit on the optical table, make sure to align the base plate
parallel to the laser beam path and that there is enough space for all stages
to move within their dynamical range. In case you do not have sufficient space
you could consider putting the base plate on mounting posts lifting the whole
unit above the surrounding components and adjust the angle plate holding the
knifes to point downward in order to reach the beam path.

Finally you need to connect all stages to their TDC001_ *dc servo controller*
cubes via the 15-pin D-type connector cables (2.5m extension cables are
available). The cubes are labeled with a single character identifying the stage
they are meant to control (e.g. 'V' = vertical). On the opposite side of each
cube, you need to connect the power supply and the USB data cable connected to
the computer.

The photodiode
~~~~~~~~~~~~~~
The **intensity measurement** is done via a DET100A_ photodiode read by an
Arduino_ microcontroller board. Place the photodiode in the beam path behind
the knifes dynamic movement area and make sure the undisturbed beam is fully
encompassed by the diodes active area. In case of larger beams you might need
to use a focusing lens right in front the photodiode (i.e. behind the knifes!)
to achieve this. You are free to mount any optical filter appropriate for your
setup on the internal *SM1* thread.

.. warning:: Don't forget to switch on the photodiode!

Connect the photodiode to the voltage divider board using the appropriate input
(0..5V or 0..15V) and connect the corresponding output to the Arduino board.
Use **A3** for signal input (blue wire) and one of **GND** for grounding
(black wire).

.. figure:: img/arduino.jpg
    :align: center
    :width: 50%
    
    Pins to use on the Ardiuno Uno microcontroller board

Finally connect the Arduino to the computer via standard USB cable. There is no
need to provide an extra voltage supply for the Arduino!


Using the software
------------------
At this point you are ready to launch the software and start taking
measurements. If the software is not yet installed, please refer to
:ref:`Installation <installation>` chapter.

Getting started
~~~~~~~~~~~~~~~
The software is started via command line by executing

.. code:: shell

    $> kestart

The application then reads the config file (see :ref:`below <config>`) and
starts initializing all devices.

.. note:: This may take up to half a minute due to the homing procedure of the
    stages

If everything went fine, the main GUI *Main Window* will show up leaving you
with basically two options

Start a New Run
    This will open the *Measurement Window* allowing you to specify
    parameters like resolution, range and direction and then start a beam width
    measurement.

Start a Noise Run
    This will open the *Noise Window* allowing you to take continuous intensity
    measurements without moving the stages at all in order to estimate readout
    noise or laser power fluctuations.

The purpose of the *Main Window* is to trigger the measurements mentioned above,
to collect the data returned from the them and finally (but optionally) perform
an estimation of the beam waist from the collected data.

Measurement Window
~~~~~~~~~~~~~~~~~~
The *Measurement Window* (see screenshot below) is the place where the actual
**width measurement** is taking place. The sliders and boxes in the upper right
allow you to define the displacement parameters for the width measurement to
perform:

Limits Start/Stop
    Start and end position of the dynamic stage (either horizontal or vertical)
    relative to its home position. This will automatically adjust the step size.

z-Position
    Displacement of the longitudinal stage along the beam path relative to its
    home position.

Step
    Defining the resolution by adjusting the step size of the dynamic stage or
    the total number of points to take between Start- and Stop-Limits.

Orientation
    Defining the dynamic stage by either choosing the horizontal or vertical
    oriented stage.

.. figure:: img/run_window.png
    :align: center
    :width: 50%

    The Measurement Window after performing a preview run

Hitting the **Run** button will trigger a new width measurement given the
parameters specified on the right. The plot below automatically updates when a
new data point gets available. At the end of the measurement the beam width is
estimated and a summary is given in the table.

.. note:: You can always stop a running measurement by hitting the **Stop**
    button.

The **Preview** button triggers a width measurement with low resolution (i.e.
1mm step size over the full range of 25mm) to help you setting the actual
measurement parameters on the right.

The **Save** button triggers the storing of the raw data in an ASCII file
format and saves the plot as .pdf file next to it (saving location is
determined by the :ref:`config file <config>`). This action closes the
*Measurment Window* and returns to the *Main Window* where the new data is
added to the table as well as width plot is being updated.

.. note:: Double clicking on a dataset in the *Main Windows* table brings back
    the *Measurement Window* showing the complete width dataset.

Noise Window
~~~~~~~~~~~~
The *Noise Window* (see screenshot below) allows you to take continuous
intensity measurements without utilizing the motorized stages. This is useful
to estimate the readout noise or monitor laser power fluctuations.

.. figure:: img/noise_window.png
    :align: center
    :width: 50%
    
    The Noise Window after taking 1000 data points

Taking a noise measurement is fairly simple, you hit the **Start** button the
software starts recording data points with about 10 Hz (please note that this
might vary depending on the performance of your computer). The timeline is
shown in the lower half and the histogram in the upper half of the window, both
being updated every 10 data points (= every 1s).

The recording will continue until you hit the **Stop** button or the desired
amount of data points has been collected when specified with the **Stop after**
option.

The **Show Saturation** option toggles the plotting of a horizontal line in the
timeline plot indicating the saturation level of the Arduinos builtin ADC.

Saving the noise data works just the way as saving a width measurement described
above.

Main Window
~~~~~~~~~~~
The *Main Window* shows the collected data from the width measurements and
lists them in a table. The estimated widths are further shown in a plot in the
upper half.

.. figure:: img/main_window.png
    :align: center
    :width: 50%
    
    The Main Window after performing several width measurements and fitting of
    the waist function.

The **Save Widths** button creates a summary ASCII file listing the estimated
widths of all datasets (in principle this is redundant information since they
are contained in each single measurements data file but it is convenient to
have a summary).


Config file
~~~~~~~~~~~
.. _config:

The config file allows you to set runtime specific parameters of things that do
not change during execution of the software. When the application is launched,
it looks for a text file called ``KnifeEdge.conf`` in the main code directory
(this may change in the future). The file contains several sections each with a
set of options and is checked for consistency and completeness when read by the
application. The following listing shows the default settings:

.. literalinclude:: ../../KnifeEdge.conf
    :linenos:
    :language: shell

The allowed values including description are given below:

[general]
`````````
path
    base path where data files are written to

demo
    [True|False]: Disable all devices and generate fake datasets for
    demonstration purposes

waistBasedOn
    [width|width9010]: Set the method of width estimation used to fit the beam
    waist function (see :ref:`Estimating the beam width <width_estimation>`)

[stageH] / [stageV] / [stageZ]
``````````````````````````````
active
    [True|False]: Set this stage active. Non-active stage cannot be used in
    within the application.

serial
    serial number of the TDC001_ controller cube attached to this stage.

[arduino]
`````````
port
    the unix filepath pointing to the serial port of the arduino (see e.g.
    https://wiki.archlinux.org/index.php/Arduino#Accessing_serial)

saturation
    maximum number of ADC counts this Arduino is returning (currently 2^10 =
    1024 bit)

.. warning:: Making inappropriate changes to the config file might result in
    the software not working properly


Troubleshooting
---------------

I don't observe any intensity value except for 0 or 1023
    * Check that the photodiode is switched on
    * Check the wiring (input/output) of the voltage divider board
    * Make sure you use *A3* and *GND* at the Arduino
    * Make sure you are using appropriate filters at the photodiode

The *Measurement Window* is not moving the stage
    * Hit the Stop button and try again (reinitializes the stage)
    * Close the window and try again (initializes a new measurement window)
    * Reconnect the power plug of the corresponding controller cube and move
      the stage manually to its home position (resets the firmware to EPROM
      defaults)

The power profile looks clipped at the top
    * Check that the Arduino is not saturating (clipping at 1024)
    * Check that the photodiode is not saturating (add more filters)

.. _MTS50: http://www.thorlabs.de/thorProduct.cfm?partNumber=MTS50/M-Z8
.. _Z825B: http://www.thorlabs.de/thorProduct.cfm?partNumber=Z825B
.. _TDC001: https://thorlabs.com/thorProduct.cfm?partNumber=TDC001
.. _DET100A: https://thorlabs.com/thorProduct.cfm?partNumber=DET100A
.. _Arduino: http://www.arduino.cc/en/Main/ArduinoBoardUno


