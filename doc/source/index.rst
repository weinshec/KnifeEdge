.. KnifeEdge documentation master file, created by
   sphinx-quickstart on Thu May 28 23:20:48 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the KnifeEdge documentation!
=======================================

Welcome to the documentation of the KnifeEdge python application. This module
is designed to perform beam width and quality measurements of approximate
Gaussian laser beams.


Using the KnifeEdge
-------------------
**Start here**, if you are new to KnifeEdge measurements and want to learn about
its working principle and how to actually do measurements.

.. toctree::
   :maxdepth: 2
   
   introduction
   tutorial


KnifeEdge python module
-----------------------
These articles cover the installation procedure of the KnifeEdge package and
its dependencies as well as a description of the application architecture and
the actual code documentation.

.. toctree::
   :maxdepth: 2

   installation
   code
   API



Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

