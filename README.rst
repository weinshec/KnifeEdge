KnifeEdge
=========
Knife Edge beam waist measurment tool


Description
-----------
The Knife Edge Unit is a measurement device used to obtain information on the
beam parameters of Gaussian laser beams. It can be used to simply measure the
beam diameter of collimated lasers beams or to measure the spot size of focused
laser beam and its beam quality parameter M^2. It is particular useful in
setups, where the focused beam spot size is too small or too large to be
measured with standard tools like CCD cameras.

This repository contains the software to control the Knife Edge Unit assembled
using Thorlabs motorized stages interfaced with TDC001 control cubes. For the
intensity measurement a photodiode read by an Arduino Uno microcontroler board
is used.


Documentation
-------------
A precompiled version of the documentation is available at:

http://desy.de/~weinshec/KnifeEdge/

However, this might not always be up to date. In order to get the latest
version of the documentation, you should build it on your own by executing

.. code:: shell
    $> make html

inside the ``doc`` folder. This requires sphinx_ to be installed and builds the
documentation inside ``doc/build/html`` which can easily be viewed by your web
browser.
 
.. _sphinx: http://sphinx-doc.org/index.html
